<?php

class Maksekeskus {

  function __construct($shopId, $apiSecret) {
    $this->shopId = $shopId;
    $this->apiSecret = $apiSecret;
  }
 function pay($paymentId, $amount) {
   $sign =  strtoupper(hash('sha512',(string) $this->shopId  .(string) $paymentId . (string) $amount . $this->apiSecret));

    $array = array(
      "shopId" => $this->shopId,
      "paymentId" => (string) $paymentId,
      "amount" => (string) $amount,
      "signature" => $sign,
    );
    return json_encode($array);
  }
  /**
  * Useful values: amount, PaymentId, status.
  */
  function receive() {
    if (isset($_POST['json'])) {
      $json = json_decode($_POST['json']);
     // if (strtoupper(hash('sha512', (string) $json->reference . (string) $json->amount . (string) $json->status . $this->apiSecret)) === $json->signature) {
      if (strtoupper(hash('sha512', (string) $json->amount . (string) $json->currency . (string) $json->reference . (string) $json->transaction . (string) $json->status . $this->apiSecret)) === $json->signature) {
        return $json;
      } else {
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }
}
?>
