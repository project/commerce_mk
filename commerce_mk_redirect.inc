<?php

/**
 * Hook function for succesful order.
 */
function commerce_mk_return() {
  // Get the _POST data from maksekeskus.
  include_once('maksekeskus.class.php');
  if (isset($_POST['json'])) {
    $json = json_decode($_POST['json']);
  } else {
    drupal_set_title('Maksekeksus: Payment error!');
    return t('Wrong POST data!');
  }
  $order = commerce_order_load($json->reference);
  // Load payment method.
  if($order) {
    $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
    $shopId = $payment_method['settings']['merchant_id'];
    $apiSecret = $payment_method['settings']['api_key'];

    $payment = new Maksekeskus($shopId, $apiSecret);
    $received = $payment->receive();
  } else {
    drupal_set_title('Maksekeksus: Payment error!');
    return t("This order id doesn't exist!");
  }
  if (!$received->status) {
    drupal_set_title('Maksekeksus: Payment error!');
    return t("Something went wrong with payment data or signature!");
     commerce_payment_redirect_pane_previous_page($order);
  }
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  // If transaction id exists load it.
  $query = new EntityFieldQuery;
  $result = $query
    ->entityCondition('entity_type', 'commerce_payment_transaction')
    ->propertyCondition('order_id', $order->order_id)
    ->propertyOrderBy('transaction_id', 'DESC')
    ->range(0, 1)
    ->execute();

  if ($transaction_id = key($result['commerce_payment_transaction'])) {
    $transaction = commerce_payment_transaction_load($transaction_id);
  } else {
    $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
  }
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->currency_code = $wrapper->commerce_order_total->currency_code->value();

  if ($received->status == 'PAID' || $received->status == 'RECEIVED' || $received->status == 'COMPLETED') {
    $transaction->amount = $wrapper->commerce_order_total->amount->value();
    $transaction->remote_status = $received->status;
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->message = 'Payment received at @date';
    $transaction->message_variables = array(
      '@date' => date('d-m-Y H:i:s', REQUEST_TIME)
    );
    commerce_payment_transaction_save($transaction);
    commerce_checkout_complete($order);
    return t("Your payment has been submitted. Thank you!");
  }
  elseif ($received->status == 'CANCELLED') {
    $order->status = 'checkout_review';
    commerce_order_save($order);
    $transaction->amount = 0;
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->message = t('Payment was cancelled');
    $transaction->remote_status = $received->status;
    commerce_payment_transaction_save($transaction);
    $uri = commerce_checkout_order_uri($order);
    drupal_goto($uri);
    return FALSE;
  }
}
